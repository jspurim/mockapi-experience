package org.mockapi.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockapi.util.JsonBuilder;

import static com.jayway.restassured.RestAssured.*;
import static com.jayway.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class ContactsAPIUnitTest {

	private static final String BASE_URL = "/metapi/api";
	private static final String METAPI_API_CLEAR = "/metapi/api/clear";

	private long getFirstContactId() {
		long toDelete = get(BASE_URL+"/contacts")
			.then()
			.extract()
				.jsonPath().getLong("[1].id");
		return toDelete;
	}
	private long getSecondContactId() {
		long toDelete = get(BASE_URL+"/contacts")
			.then()
			.extract()
				.jsonPath().getLong("[0].id");
		return toDelete;
	}
	
	private long getFirstMessageId() {
		long toDelete = get(BASE_URL+"/messages")
			.then()
			.extract()
				.jsonPath().getLong("[1].id");
		return toDelete;
	}
	
	private long getFirstProviderId() {
		long toDelete = get(BASE_URL+"/providers")
			.then()
			.extract()
				.jsonPath().getLong("[0].id");
		return toDelete;
	}
	
	private long getFirstGroupId() {
		long toDelete = get(BASE_URL+"/groups")
			.then()
			.extract()
				.jsonPath().getLong("[0].id");
		return toDelete;
	}
	
	private long getSecondGroupId() {
		long toDelete = get(BASE_URL+"/groups")
			.then()
			.extract()
				.jsonPath().getLong("[1].id");
		return toDelete;
	}
	
	@Before
	public void setUp() throws JSONException {
		//Contactos
		post(METAPI_API_CLEAR);
		JSONObject contact1 = new JSONObject();
		contact1.put("name", "test");
		contact1.put("email", "test@gmail.com");
		given().contentType("application/json").body(contact1.toString()).post(BASE_URL + "/contacts");

		JSONObject contact2 = new JSONObject();
		contact2.put("name", "test2");
		contact2.put("email", "test2@gmail.com");
		given().contentType("application/json").body(contact2.toString()).post(BASE_URL + "/contacts");
		
		//Proveedores
		JSONObject provider1 = new JSONObject();
		provider1.put("name", "google");
		given().contentType("application/json").body(provider1.toString()).post(BASE_URL + "/providers");
		
		JSONObject provider2 = new JSONObject();
		provider2.put("name", "facebook");
		given().contentType("application/json").body(provider2.toString()).post(BASE_URL + "/providers");
		
		//Grupos
		JSONObject group1 = new JSONObject();
		group1.put("name", "friends");
		given().contentType("application/json").body(group1.toString()).post(BASE_URL + "/groups");
		
		JSONObject group2 = new JSONObject();
		group2.put("name", "work");
		given().contentType("application/json").body(group2.toString()).post(BASE_URL + "/groups");
		
		long contact = getFirstContactId();
		long group = getFirstGroupId();
		
		JSONObject contactJson = new JSONObject();
		contactJson.put("id", contact);
		given().contentType("application/json").body(contactJson.toString()).put(BASE_URL+"/groups/"+group+"/contacts");
		
		//Messages
		JSONObject msg1 = new JSONObject();
		msg1.put("text", "hello");
		given().contentType("application/json").body(msg1.toString()).post(BASE_URL+"/contacts/"+contact+"/messages");
		
		JSONObject msg2 = new JSONObject();
		msg2.put("text", "bye");
		given().contentType("application/json").body(msg2.toString()).post(BASE_URL+"/contacts/"+contact+"/messages");
		
		
	}
	//@After
	public void tearDown() {
		post(METAPI_API_CLEAR);
	}

	@Test
	public void testListContacts() throws IOException {
		//US1.1 Como usuario quiero listar mis contactos
		expect()
		.body("size", is(2))
		.body("[0].id", notNullValue())
		.body("[0].name", equalTo("test"))
		.body("[0].email", equalTo("test@gmail.com"))
		.body("[1].id", notNullValue())
		.body("[1].name", equalTo("test2"))
		.body("[1].email", equalTo("test2@gmail.com"))
		.when()
		.get(BASE_URL+"/contacts");
	}
	
	@Test
	public void testDeleteContacts() throws IOException {
		//US1.2 Como usuario quiero eliminar mis contactos
		long toDelete = getSecondContactId();


		delete(BASE_URL+"/contacts/"+toDelete);
		expect()
		.body("size", is(1))
		.body("[0].id", notNullValue())
		.body("[0].name", equalTo("test2"))
		.body("[0].email", equalTo("test2@gmail.com"))
		.when()
		.get(BASE_URL+"/contacts");
	}
	
	@Test
	public void testDeleteContactWithMessagesShouldFail() throws IOException {
		//US1.2 Como usuario quiero eliminar mis contactos
		long toDelete = getFirstContactId();


		expect()
		.statusCode(400)
		.when()
		.delete(BASE_URL+"/contacts/"+toDelete);
		expect()
		.body("size", is(2))
		.body("[0].id", notNullValue())
		.body("[0].name", equalTo("test"))
		.body("[0].email", equalTo("test@gmail.com"))
		.body("[1].name", equalTo("test2"))
		.body("[1].email", equalTo("test2@gmail.com"))
		.when()
		.get(BASE_URL+"/contacts");
	}



	
	@Test
	public void testAddContacts() throws IOException, JSONException {
		//US2.1 Como usuario quiero agregar contactos
		JSONObject contact1 = new JSONObject();
		contact1.put("name", "grayfox");
		contact1.put("email", "grayfox@gmail.com");
		given().contentType("application/json").body(contact1.toString()).post(BASE_URL + "/contacts");
		expect()
		.body("size", is(3))
		.body("[2].id", notNullValue())
		.body("[2].name", equalTo("grayfox"))
		.body("[2].email", equalTo("grayfox@gmail.com"))
		.when()
		.get(BASE_URL+"/contacts");
	}
	
	@Test
	public void testAddAccount() throws IOException, JSONException {
		//US2.2 Como usuario quiero agregar cuentas a mis contactos
		long user = getFirstContactId();
		long provider = getFirstProviderId();
		
		JSONObject account1 = new JSONObject();
		account1.put("email", "grayfox@gmail.com");
		account1.put("provider", provider);
		given().contentType("application/json").body(account1.toString())
		.post(BASE_URL+"/contacts/"+user+"/accounts");
		
		expect()
		.body("size", is(1))
		.body("[0].id", notNullValue())
		.body("[0].email", equalTo("grayfox@gmail.com"))
		.when()
		.get(BASE_URL+"/contacts/"+user+"/accounts");
		expect()
		.body("size", is(1))
		.body("[0].id", notNullValue())
		.body("[0].email", equalTo("grayfox@gmail.com"))
		.when()
		.get(BASE_URL+"/providers/"+provider+"/accounts");
		
	}
	
	@Test
	public void testSecondContactToAccountShouldFail() throws IOException, JSONException {
		//US2.3 Solo se permite un contacto por cuenta
		long user = getFirstContactId();
		long user2 = getFirstContactId();
		long provider = getFirstProviderId();
		
		JSONObject account1 = new JSONObject();
		account1.put("email", "grayfox@gmail.com");
		account1.put("provider", provider);
		given().contentType("application/json").body(account1.toString())
		.post(BASE_URL+"/contacts/"+user+"/accounts");
		
		given().contentType("application/json").body(account1.toString())
		.expect()
		.statusCode(404)
		.when()
		.put(BASE_URL+"/contacts/"+user2+"/accounts");
	}
	
	@Test
	public void testAddAccountWithMaldormedEmailShouldFail() throws IOException, JSONException {
		//US2.4 Los emails tienen que tener un formato válido
		long user = getFirstContactId();
		long provider = getFirstProviderId();
		
		JSONObject account1 = new JSONObject();
		account1.put("email", "grayfoxgmail.com");
		account1.put("provider", provider);
		
		JSONObject account2 = new JSONObject();
		account2.put("email", "grayfox@gmail@.com");
		account2.put("provider", provider);
		
		JSONObject account3 = new JSONObject();
		account3.put("email", "grayfox@gmail.com!");
		account3.put("provider", provider);
		
		given().contentType("application/json").body(account1.toString())
		.expect()
		.statusCode(400)
		.when()
		.post(BASE_URL+"/contacts/"+user+"/accounts");
		
		given().contentType("application/json").body(account2.toString())
		.expect()
		.statusCode(400)
		.when()
		.post(BASE_URL+"/contacts/"+user+"/accounts");
		
		given().contentType("application/json").body(account3.toString())
		.expect()
		.statusCode(400)
		.when()
		.post(BASE_URL+"/contacts/"+user+"/accounts");
	}
	
	@Test
	public void testListProviders() throws IOException {
		//US3.1 Como usuario quiero listar los proveedores de cuentas
		expect()
		.body("size", is(2))
		.body("[0].id", notNullValue())
		.body("[0].name", equalTo("google"))
		.body("[1].id", notNullValue())
		.body("[1].name", equalTo("facebook"))
		.when()
		.get(BASE_URL+"/providers");
	}
	
	@Test
	public void testAddProviders() throws IOException, JSONException {
		//US3.2 Como usuario quiero agregar proveedores de cuentas
		
		//Proveedores
		JSONObject provider1 = new JSONObject();
		provider1.put("name", "twitter");
		given().contentType("application/json").body(provider1.toString()).post(BASE_URL + "/providers");
		expect()
		.body("size", is(3))
		.body("[0].id", notNullValue())
		.body("[0].name", equalTo("google"))
		.body("[1].id", notNullValue())
		.body("[1].name", equalTo("facebook"))
		.body("[2].id", notNullValue())
		.body("[2].name", equalTo("twitter"))
		.when()
		.get(BASE_URL+"/providers");
	}
	
	@Test
	public void testDeleteProviders() throws IOException, JSONException {
		//US3.3 Como usuario quiero eliminar proveedores de cuentas
		
		long toDelete = getFirstProviderId();
		delete(BASE_URL+"/providers/"+toDelete);
		
		expect()
		.body("size", is(1))
		.body("[0].id", notNullValue())
		.body("[0].name", equalTo("facebook"))
		.when()
		.get(BASE_URL+"/providers");
	}
	
	@Test
	public void testListGroups() throws IOException, JSONException {
		//US4.1 Como usuario quiero listar grupos
		
		
		expect()
		.body("size", is(2))
		.body("[0].id", notNullValue())
		.body("[0].name", equalTo("friends"))

		.body("[1].id", notNullValue())
		.body("[1].name", equalTo("work"))
		.when()
		.get(BASE_URL+"/groups");
	}
	
	@Test
	public void testDeleteGroup() throws IOException, JSONException {
		//US4.2 Como usuario quiero borrar un grupo
		
		long toDelete = getSecondGroupId();
		expect().statusCode(200).when().delete(BASE_URL+"/groups/"+toDelete);
		expect()
		.body("size", is(1))
		.body("[0].id", notNullValue())
		.body("[0].name", equalTo("friends"))
		.when()
		.get(BASE_URL+"/groups");
	}
	
	@Test
	public void testDeleteGroupWithContactsShouldFail() throws IOException, JSONException {
		//US4.3 No se pueden borrar grupos con miembros
		
		long toDelete = getFirstGroupId();
		expect().statusCode(400).when().delete(BASE_URL+"/groups/"+toDelete);
		expect()
		.body("size", is(2))
		.body("[0].id", notNullValue())
		.body("[0].name", equalTo("friends"))
		.body("[1].id", notNullValue())
		.body("[1].name", equalTo("work"))
		.when()
		.get(BASE_URL+"/groups");
	}
	
	@Test
	public void testAddGroups() throws IOException, JSONException {
		//US4.4 Como usuario quiero agregar un grupo
		JSONObject group1 = new JSONObject();
		group1.put("name", "family");
		given().contentType("application/json").body(group1.toString()).post(BASE_URL + "/groups");
		expect()
		.body("size", is(3))
		.body("[0].id", notNullValue())
		.body("[0].name", equalTo("friends"))

		.body("[1].id", notNullValue())
		.body("[1].name", equalTo("work"))
		
		.body("[2].id", notNullValue())
		.body("[2].name", equalTo("family"))
		.when()
		.get(BASE_URL+"/groups");
	}
	
	@Test
	public void testAddTooManyGroupsShouldFail() throws IOException, JSONException {
		//US4.5 La aplicacion admitira un máximo de 5 grupos
		JSONObject group1 = new JSONObject();
		group1.put("name", "family");
		given().contentType("application/json").body(group1.toString()).expect().statusCode(200).when().post(BASE_URL + "/groups");
		given().contentType("application/json").body(group1.toString()).expect().statusCode(200).when().post(BASE_URL + "/groups");
		given().contentType("application/json").body(group1.toString()).expect().statusCode(200).when().post(BASE_URL + "/groups");
		given().contentType("application/json").body(group1.toString()).expect().statusCode(400).when().post(BASE_URL + "/groups");
		expect()
		.body("size", is(5))
		.when()
		.get(BASE_URL+"/groups");
	}
	
	
	@Test
	public void testListGroupMembers() throws IOException, JSONException {
		//US5.1 Como usuario quiero listar los miembros de un grupo
		long group=getFirstGroupId();
		expect()
		.body("size", is(1))
		.body("[0].id", notNullValue())
		.body("[0].name", equalTo("test2"))

		.when()
		.get(BASE_URL+"/groups/"+group+"/contacts");
	}
	
	@Test
	public void testAddGroupMembers() throws IOException, JSONException {
		//US5.2 Como usuario agregar gente a un grupo
		long group=getFirstGroupId();
		
		JSONObject contactJson = new JSONObject();
		contactJson.put("id", getSecondContactId());
		given().contentType("application/json").body(contactJson.toString()).put(BASE_URL+"/groups/"+group+"/contacts");
		
		expect()
		.body("size", is(2))
		.body("[1].id", notNullValue())
		.body("[1].name", equalTo("test2"))
		.body("[0].id", notNullValue())
		.body("[0].name", equalTo("test"))

		.when()
		.get(BASE_URL+"/groups/"+group+"/contacts");
	}

	
	@Test
	public void testRemoveGroupMembers() throws IOException, JSONException {
		//US5.3 Como usuario eliminar gente de un grupo
		long group=getFirstGroupId();
		delete(BASE_URL+"/groups/"+group+"/contacts/"+getFirstContactId());
		
		expect()
		.body("size", is(0))

		.when()
		.get(BASE_URL+"/groups/"+group+"/contacts");
	}
	
	
	@Test
	public void testAddTooManyGroupMembersShouldFail() throws IOException, JSONException {
		//US5.4 Un grupo tiene un máximo de 10 miembros
		
		//Crear contactos extra
		JSONObject contact = new JSONObject();
		contact.put("name", "dummy");
		contact.put("email", "dummy@gmail.com");
		
		given().contentType("application/json").body(contact.toString()).post(BASE_URL + "/contacts");
		given().contentType("application/json").body(contact.toString()).post(BASE_URL + "/contacts");
		given().contentType("application/json").body(contact.toString()).post(BASE_URL + "/contacts");
		given().contentType("application/json").body(contact.toString()).post(BASE_URL + "/contacts");
		given().contentType("application/json").body(contact.toString()).post(BASE_URL + "/contacts");
		
		given().contentType("application/json").body(contact.toString()).post(BASE_URL + "/contacts");
		given().contentType("application/json").body(contact.toString()).post(BASE_URL + "/contacts");
		given().contentType("application/json").body(contact.toString()).post(BASE_URL + "/contacts");
		given().contentType("application/json").body(contact.toString()).post(BASE_URL + "/contacts");
		given().contentType("application/json").body(contact.toString()).post(BASE_URL + "/contacts");
		
		given().contentType("application/json").body(contact.toString()).post(BASE_URL + "/contacts");
		
		
		long group=getSecondGroupId();
		
		String contactsJson = get(BASE_URL+"/contacts")
		.then()
		.extract()
		.asString();
		JSONArray contacts = new JSONArray(contactsJson);
		for(int i=0;i<10;i++){
			given().contentType("application/json")
			.body(contacts.get(i).toString())
			.expect()
			.statusCode(200)
			.when()
			.put(BASE_URL+"/groups/"+group+"/contacts");
		}
		given().contentType("application/json")
		.body(contacts.get(10).toString())
		.expect()
		.statusCode(400)
		.when()
		.put(BASE_URL+"/groups/"+group+"/contacts");

		expect()
		.body("size", is(10))
		.when()
		.get(BASE_URL+"/groups/"+group+"/contacts");
	}

	@Test
	public void testListMessages() throws IOException, JSONException {
		//US6.1 Como usuario quiero listar los mensajes de un contacto
		
		expect()
		.body("size", is(2))
		.body("[0].id", notNullValue())
		.body("[0].text", equalTo("hello"))
		
		.body("[1].id", notNullValue())
		.body("[1].text", equalTo("bye"))
		.when()
		.get(BASE_URL+"/contacts/"+getFirstContactId()+"/messages");
	}
	
	@Test
	public void testDeleteMessages() throws IOException, JSONException {
		//US6.2 Como usuario quiero eliminar mensajes
		delete(BASE_URL+"/messages/"+getFirstMessageId());
		
		expect()
		.body("size", is(1))
		.body("[0].id", notNullValue())
		.body("[0].text", equalTo("hello"))
		.when()
		.get(BASE_URL+"/contacts/"+getFirstContactId()+"/messages");
	}
	
	@Test
	public void testSendMessages() throws IOException, JSONException {
		//US7.1 Como usuario quiero enviar mensajes a un contacto

		JSONObject msg1 = new JSONObject();
		msg1.put("text", "testing");
		given().contentType("application/json").body(msg1.toString()).expect().statusCode(200).when().post(BASE_URL+"/contacts/"+getFirstContactId()+"/messages");
		expect()
		.body("size", is(3))
		.body("[0].id", notNullValue())
		.body("[0].text", equalTo("hello"))

		.body("[1].id", notNullValue())
		.body("[1].text", equalTo("bye"))

		.body("[2].id", notNullValue())
		.body("[2].text", equalTo("testing"))
		.when()
		.get(BASE_URL+"/contacts/"+getFirstContactId()+"/messages");
	}
	
	@Test
	public void testSendLongMessageShouldFail() throws IOException, JSONException {
		//US7.2 El texto de un mensaje no podra superar los 300 caracteres.
		
		String longtext = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ipsum metus, "
				+ "imperdiet ac libero quis, sollicitudin ultrices felis. Donec semper scelerisque augue."
				+ " Fusce elementum neque eget dignissim tristique. Fusce efficitur accumsan elit sit amet"
				+ " blandit. Sed cursus pretium lectus eu congue. Nam magna ligula, placerat et pretium a,"
				+ " rutrum nec dui. Nulla massa sapien, dapibus quis enim sed, auctor accumsan metus."
				+ " Etiam a gravida est. Duis ultricies cursus elementum. Aenean sollicitudin felis dapibus,"
				+ " tempor eros finibus, cursus mauris. Ut sit amet egestas nulla, vel laoreet elit. "
				+ "Sed id risus ut ipsum gravida dignissim consectetur vitae libero. Praesent egestas leo "
				+ "vitae nunc pretium, vitae volutpat odio dapibus. Aenean consequat dolor est.";
		JSONObject msg1 = new JSONObject();
		msg1.put("text", longtext);
		given().contentType("application/json").body(msg1.toString())
		.expect()
		.statusCode(400)
		.when()
		.post(BASE_URL+"/contacts/"+getFirstContactId()+"/messages");

		expect()
		.body("size", is(2))
		.body("[0].id", notNullValue())
		.body("[0].text", equalTo("hello"))

		.body("[1].id", notNullValue())
		.body("[1].text", equalTo("bye"))
		.get(BASE_URL+"/contacts/"+getFirstContactId()+"/messages");
	}
	

}
