package org.mockapi.util;

import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.metapi.metabase.MetabaseObject;
import org.metapi.metabase.hsqldb.MetabaseHsqldb;

public class MetabaseUtil {

	private static MetabaseHsqldb metabase = null;

	public static JSONObject generateJson(MetabaseObject obj) throws JSONException{
		return new JSONObject(obj.getRepresentation()).put("id", obj.getId());
	}

	public static JSONArray generateJson(List<MetabaseObject> list) throws JSONException{
		JSONArray ret = new JSONArray();
		for(MetabaseObject obj : list){
			ret.put(new JSONObject(obj.getRepresentation()).put("id", obj.getId()));
		}
		return ret;
	}

	public static MetabaseHsqldb getMetabaseInstance() throws Exception {
		if(metabase == null){
			metabase = new MetabaseHsqldb("db");
		}
		return metabase;
	}

}
