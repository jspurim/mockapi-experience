package org.mockapi.experiments;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Set;
import java.util.HashSet;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.apache.commons.io.IOUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.metapi.metabase.MetabaseException;
import org.metapi.metabase.hsqldb.MetabaseHsqldb;
import org.mockapi.util.MetabaseUtil;
import org.springframework.web.bind.annotation.RequestBody;

@Path("/experiments")
@Produces("application/json")
public class ExperimentResource {

	private MetabaseHsqldb metabase;
	private Pattern pattern;

	public ExperimentResource() throws Exception{
		metabase = MetabaseUtil.getMetabaseInstance();
		pattern = Pattern.compile("[a-zA-Z0-9-]+");
	}

	@POST
	public Response addExperiment(@RequestBody InputStream body) throws JSONException, MetabaseException, IOException{
		JSONObject exp = new JSONObject(IOUtils.toString(body));
		if(!exp.has("deliverables")){
			exp.put("deliverables", new JSONArray());
		}
		JSONArray deliverables = exp.getJSONArray("deliverables");
		Set<String> deliverablesSet = new HashSet<String>();
		for(int i=0;i<deliverables.length();i++){
			String deliverable = deliverables.getJSONObject(i).getString("name");
			if(deliverablesSet.contains(deliverable)){
				return Response.status(400).build();
			}
			deliverablesSet.add(deliverable);
			Matcher matcher = pattern.matcher(deliverable);
			if(!matcher.matches()){
		 		return Response.status(400).build();
		 	}
		}


		return Response.ok(
				MetabaseUtil.generateJson(this.metabase.save("Experiment",
						exp.toString())).toString()).build();
	}

	@GET
	public Response listExperiments() throws JSONException, MetabaseException, IOException{
		return Response.ok(MetabaseUtil.generateJson(this.metabase.getAll("Experiment")).toString()).build();

	}

	@GET
	@Path("/{id}")
	public Response getExperiment(@PathParam("id") long id) throws JSONException, MetabaseException, IOException{
		JSONObject experiment = MetabaseUtil.generateJson(this.metabase.getById("Experiment", id));
		if(experiment == null){
			return Response.status(404).build();
		}
		JSONArray instances = MetabaseUtil.generateJson(this.metabase.getAssociatedObjects("Experiment", "instances", "Instance", id));
		for(int i=0;i <instances.length();i++){
			JSONObject instance = instances.getJSONObject(i);
			JSONObject subject = MetabaseUtil.generateJson(this.metabase.getById("Subject", instance.getLong("subject_id")));
			instance.put("subject", subject);
		}
		experiment.put("instances", instances);
		return Response.ok(experiment.toString()).build();
	}

	@DELETE
	@Path("/{id}")
	public Response deleteExperiment(@PathParam("id") long id) throws JSONException, MetabaseException, IOException{
		JSONObject experiment = MetabaseUtil.generateJson(this.metabase.getById("Experiment", id));
		JSONArray instances = MetabaseUtil.generateJson(this.metabase.getAssociatedObjects("Experiment", "instances", "Instance", id));
		if(instances.length() > 0){
			return Response.status(400).build();
		}
		if(experiment == null){
			return Response.status(404).build();
		}
		this.metabase.delete("Experiment", id);
		return Response.ok().build();
	}
}
