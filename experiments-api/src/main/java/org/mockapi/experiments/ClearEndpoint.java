package org.mockapi.experiments;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.metapi.metabase.MetabaseException;
import org.mockapi.util.MetabaseUtil;

@Path("/clear")
public class ClearEndpoint {
	
	@POST
	public Response clear() throws MetabaseException, Exception{
		MetabaseUtil.getMetabaseInstance().clearDatabase();
		return Response.ok().build();
	}
}
