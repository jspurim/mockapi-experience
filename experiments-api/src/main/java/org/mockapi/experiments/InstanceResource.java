package org.mockapi.experiments;

import java.io.IOException;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.commons.io.IOUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.metapi.metabase.Metabase;
import org.metapi.metabase.MetabaseException;
import org.metapi.metabase.hsqldb.MetabaseHsqldb;
import org.mockapi.util.MetabaseUtil;
import org.springframework.web.bind.annotation.RequestBody;

@Path("/instances")
@Produces("application/json")
public class InstanceResource {

	private MetabaseHsqldb metabase;

	public InstanceResource() throws Exception{
		metabase = MetabaseUtil.getMetabaseInstance();
	}


	@GET
	public Response listInstaces() throws JSONException, MetabaseException, IOException{
		return Response.ok(MetabaseUtil.generateJson(this.metabase.getAll("Instance")).toString()).build();
	}

	@POST
	public Response createInstance(@RequestBody InputStream body) throws JSONException, MetabaseException, IOException{
		JSONObject json = new JSONObject(IOUtils.toString(body));
		long expId = json.getLong("exp_id");
		long subjectId = json.getLong("subject_id");
		JSONArray instances = MetabaseUtil.generateJson(this.metabase.getAssociatedObjects("Subject", "instances", "Instance", subjectId));
		if(instances.length() >= 3){
			return Response.status(400).build();
		}
		json.put("deliverables", new JSONObject());
		json.put("description", "");
		JSONObject instance = MetabaseUtil.generateJson(this.metabase.save("Instance", json.toString()));
		this.metabase.associate("Experiment", "instances", "Instance", expId, instance.getLong("id"));
		this.metabase.associate("Subject", "instances", "Instance", subjectId, instance.getLong("id"));
		return Response.ok().build();
	}

	@PUT
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Path("/{id}")
	public Response uploadDeliverables(
				FormDataMultiPart multipart,
				@PathParam("id") long id) throws JSONException, MetabaseException, IOException{


		Map<String,List<FormDataBodyPart> > fields = multipart.getFields();
		String description = null;
		JSONObject instance = MetabaseUtil.generateJson(this.metabase.getById("Instance", id));
		JSONObject deliverables = instance.getJSONObject("deliverables");
		JSONObject experiment =  MetabaseUtil.generateJson(this.metabase.getById("Experiment", instance.getLong("exp_id")));
		JSONArray deliverablesList = experiment.getJSONArray("deliverables");

		for(Map.Entry<String, List<FormDataBodyPart>> entry : fields.entrySet()){
			if(entry.getValue().size() > 1){
				return Response.status(400).build();
			}
			if(entry.getKey().equals("description")){
				description = entry.getValue().get(0).getValue();
			} else {
				FormDataBodyPart part = entry.getValue().get(0);
				String name = part.getName();
				boolean ok  = false;
				for(int i=0;i<deliverablesList.length();i++){
					if(deliverablesList.getJSONObject(i).getString("name").equals(name)){
						ok = true;
						break;
					}
				}
				if(!ok){
					return Response.status(400).build();
				}
				String data = part.getValueAs(String.class);
				deliverables.put(name, data);
			}
		}
		if(deliverables.length() != deliverablesList.length()){
			return Response.status(400).build();
		}
		if(description == null || description.length() > 200){
			return Response.status(400).build();
		}
		instance.put("description", description);
		instance.put("deliverables", deliverables);
		this.metabase.update("Instance", id,  instance.toString());
		return Response.ok().build();
	}

	@GET
	@Path("/{id}")
	public Response getInstance(@PathParam("id") long id) throws JSONException, MetabaseException, IOException{
		JSONObject instance =  MetabaseUtil.generateJson(this.metabase.getById("Instance", id));
		if(instance == null){
			return Response.status(404).build();
		}
		JSONObject deliverables = instance.getJSONObject("deliverables");
		instance.remove("deliverables");
		JSONArray deliverablesList = new JSONArray();
		Iterator it = deliverables.keys();
		while(it.hasNext()){
			deliverablesList.put(it.next());
		}
		instance.put("deliverables", deliverablesList);
		return Response.ok(instance.toString()).build();
	}


	@GET
	@Path("/{id}/deliverables/{name}")
	public Response getDeliverables(@PathParam("id") long id,
				@PathParam("name") String name) throws JSONException, MetabaseException, IOException{
		JSONObject instance =  MetabaseUtil.generateJson(this.metabase.getById("Instance", id));
		JSONObject deliverables = instance.getJSONObject("deliverables");
		if(!deliverables.has(name)){
			return Response.status(404).build();
		}
		return Response.ok(deliverables.getString(name)).build();
	}

}
