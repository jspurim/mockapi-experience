package org.mockapi.experiments;

import java.io.IOException;
import java.io.InputStream;

import javax.ws.rs.POST;
import javax.ws.rs.GET;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.Produces;

import org.apache.commons.io.IOUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.metapi.metabase.MetabaseException;
import org.metapi.metabase.hsqldb.MetabaseHsqldb;
import org.mockapi.util.MetabaseUtil;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Path("/subjects")
@Produces("application/json")
public class SubjectResource {

	private static final String EMAIL_PATTERN =
		"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";


	private MetabaseHsqldb metabase;
  private Pattern pattern;


	public SubjectResource() throws Exception{
		metabase = MetabaseUtil.getMetabaseInstance();
		pattern = Pattern.compile(EMAIL_PATTERN);
	}

	@POST
	public Response addSubject(@RequestBody InputStream body) throws JSONException, MetabaseException, IOException{
		JSONObject subject = new JSONObject(IOUtils.toString(body));
		if(!(subject.has("name") && subject.has("surname") && subject.has("email"))){
			return Response.status(400).build();
		}
		String email = subject.getString("email");
		Matcher matcher = pattern.matcher(email);
		if(!matcher.matches()){
			return Response.status(400).build();
		}
		return Response.ok(
				MetabaseUtil.generateJson(this.metabase.save("Subject",subject.toString())).toString()).build();
	}

	@GET
	public Response listSubjects() throws JSONException, MetabaseException, IOException{
		return Response.ok(MetabaseUtil.generateJson(this.metabase.getAll("Subject")).toString()).build();
	}

	@DELETE
	@Path("/{id}")
	public Response deleteSubject(@PathParam("id") long id) throws JSONException, MetabaseException, IOException{
		JSONObject experiment = MetabaseUtil.generateJson(this.metabase.getById("Subject", id));
		if(experiment == null){
			return Response.status(404).build();
		}
		this.metabase.delete("Subject", id);
		return Response.ok().build();
	}
}
