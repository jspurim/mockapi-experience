package org.mockapi.experiments.test;

import com.jayway.restassured.http.ContentType;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockapi.util.JsonBuilder;
import static com.jayway.restassured.RestAssured.*;
import static com.jayway.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

public class ExperimentResourceTest {

	private static final String BASE_URL = "/webapi";
	private static final String METAPI_API_CLEAR = "/webapi/clear";


	@Before
	public void setUp() throws JSONException{
		post(METAPI_API_CLEAR);

		JSONArray deliverables = new JSONArray();
		JSONObject del1 = new JSONObject();
		del1.put("name", "doc");
		del1.put("type", "zip");
		JSONObject del2 = new JSONObject();
		del2.put("name", "code");
		del2.put("type", "zip");

		deliverables.put(del1);
		deliverables.put(del2);

		JSONArray deliverables2 = new JSONArray();
		JSONObject del3 = new JSONObject();
		del3.put("name", "code");
		del3.put("type", "zip");
		deliverables2.put(del3);

		//Experiments
		JSONObject exp = new JSONObject();
		exp.put("name", "exp1");
		exp.put("description", "test1");
		exp.put("code", "EXP1");
		exp.put("date",new Date(3,7,2016));

		JSONObject exp2 = new JSONObject();
		exp2.put("name", "exp2");
		exp2.put("description", "test2");
		exp2.put("code", "EXP2");
		exp2.put("date",new Date(3,7,2016));

		JSONObject exp3 = new JSONObject();
		exp3.put("name", "exp3");
		exp3.put("description", "test3");
		exp3.put("code", "EXP3");
		exp3.put("date",new Date(3,7,2016));

		JSONObject exp4 = new JSONObject();
		exp4.put("name", "exp4");
		exp4.put("description", "test4");
		exp4.put("code", "EXP4");
		exp4.put("date",new Date(3,7,2016));

		exp.put("deliverables", deliverables);
		exp2.put("deliverables", deliverables2);


		given().body(exp.toString()).post(BASE_URL+"/experiments");
		given().body(exp2.toString()).post(BASE_URL+"/experiments");
		given().body(exp3.toString()).post(BASE_URL+"/experiments");
		given().body(exp4.toString()).post(BASE_URL+"/experiments");

		//Subjects
		JSONObject subject1 = new JSONObject();
		JSONObject subject2 = new JSONObject();
		subject1.put("name","name1");
		subject1.put("surname","sname1");
		subject1.put("email","email1@gmail.com");

		subject2.put("name","name2");
		subject2.put("surname","sname2");
		subject2.put("email","email2@gmail.com");

		given().body(subject1.toString()).post(BASE_URL+"/subjects");
		given().body(subject2.toString()).post(BASE_URL+"/subjects");

		//Instances
		JSONObject expInstance1 = new JSONObject();
		expInstance1.put("exp_id", getFirstExperimentId());
		expInstance1.put("subject_id", getFirstSubjectId());
		given().body(expInstance1.toString()).post(BASE_URL+"/instances");
	}

	@After
	public void tearDown(){
		//post(METAPI_API_CLEAR);
	}

	private long getFirstExperimentId(){
		return get(BASE_URL+"/experiments")
		.then().extract().jsonPath().getLong("[0].id");
	}

	private long getSecondExperimentId(){
		return get(BASE_URL+"/experiments")
		.then().extract().jsonPath().getLong("[1].id");
	}

	private long getFirstSubjectId(){
		return get(BASE_URL+"/subjects")
		.then().extract().jsonPath().getLong("[0].id");
	}

	private long getNthExperimentId(int n){
		return get(BASE_URL+"/experiments")
		.then().extract().jsonPath().getLong("["+n+"].id");
	}

	private long getSecondSubjectId(){
		return get(BASE_URL+"/subjects")
		.then().extract().jsonPath().getLong("[1].id");
	}

	private long getFirstInstanceId(){
		return get(BASE_URL+"/instances")
		.then().extract().jsonPath().getLong("[0].id");
	}


	@Test
	public void testListExperiments() throws JSONException{
		//US1.1 Como usuario quiero listar los experimentos
		expect()
		.statusCode(200)
		.body("size", is(4))
		.body("[0].name", equalTo("exp1"))
		.body("[0].code", equalTo("EXP1"))
		.body("[0].description", equalTo("test1"))
		.body("[0].deliverables.size",is(2))
		.body("[1].name", equalTo("exp2"))
		.body("[1].code", equalTo("EXP2"))
		.body("[1].description", equalTo("test2"))
		.body("[1].deliverables.size",is(1))
		.when()
		.get(BASE_URL+"/experiments");
	}

	@Test
	public void testDeleteExperiments() throws JSONException{
		//US1.2 Como usuario quiero borrar experimentos
		expect()
		.statusCode(200)
		.when()
		.delete(BASE_URL+"/experiments/"+getSecondExperimentId());
		expect()
		.statusCode(200)
		.body("size", is(3))
		.body("[0].name", equalTo("exp1"))
		.body("[0].code", equalTo("EXP1"))
		.body("[0].description", equalTo("test1"))
		.body("[0].deliverables.size",is(2))
		.when()
		.get(BASE_URL+"/experiments");
	}

	@Test
	public void testDeleteExperimentWithSubjectShouldFail() throws JSONException{
		//US1.3 Borrar un experimento debe fallar si tiene sujetos asignados.
		expect()
		.statusCode(400)
		.when()
		.delete(BASE_URL+"/experiments/"+getFirstExperimentId());
		expect()
		.statusCode(200)
		.body("size", is(4))
		.when()
		.get(BASE_URL+"/experiments");
	}

	@Test
	public void testGetExperimentDetails() throws JSONException{
		//US2 Como usuario deseo ver el detalle de ejecucion de un experimento
		expect()
		.statusCode(200)
		.body("name", equalTo("exp1"))
		.body("code", equalTo("EXP1"))
		.body("description", equalTo("test1"))
		.body("deliverables.size",is(2))
		.body("instances.size", is(1))
		.body("instances[0].subject.name", equalTo("name1"))
		.body("instances[0].subject.surname", equalTo("sname1"))
		.when()
		.get(BASE_URL+"/experiments/"+getFirstExperimentId());
	}

	@Test
	public void testAddParticipant() throws JSONException{
		//US3 Como usuario deseo agregar participantes a un experimento
		//US5 Como usuario debo poder asignar participantes a un experimento
		JSONObject instance = new JSONObject();
		instance.put("subject_id", getSecondSubjectId());
		instance.put("exp_id", getFirstExperimentId());
		given()
		.body(instance.toString())
		.post(BASE_URL+"/instances");

		expect()
		.statusCode(200)
		.body("instances.size", is(2))
		.when()
		.get(BASE_URL+"/experiments/"+getFirstExperimentId());
	}

	@Test
	public void testAddParticipantToFourthExperimentShouldFail() throws JSONException{
		//US3 Como usuario deseo agregar participantes a un experimento
		//US5 Como usuario debo poder asignar participantes a un experimento
		JSONObject instance = new JSONObject();
		instance.put("subject_id", getSecondSubjectId());
		instance.put("exp_id", getNthExperimentId(0));
		given()
		.body(instance.toString())
		.expect()
		.statusCode(200)
		.when()
		.post(BASE_URL+"/instances");

		instance.put("exp_id", getNthExperimentId(1));
		given()
		.body(instance.toString())
		.expect()
		.statusCode(200)
		.when()
		.post(BASE_URL+"/instances");

		instance.put("exp_id", getNthExperimentId(2));
		given()
		.body(instance.toString())
		.expect()
		.statusCode(200)
		.when()
		.post(BASE_URL+"/instances");

		instance.put("exp_id", getNthExperimentId(3));
		given()
		.body(instance.toString())
		.expect()
		.statusCode(400)
		.when()
		.post(BASE_URL+"/instances");

		expect()
		.statusCode(200)
		.body("instances.size", is(2))
		.when()
		.get(BASE_URL+"/experiments/"+getNthExperimentId(0));

		expect()
		.statusCode(200)
		.body("instances.size", is(1))
		.when()
		.get(BASE_URL+"/experiments/"+getNthExperimentId(1));

		expect()
		.statusCode(200)
		.body("instances.size", is(1))
		.when()
		.get(BASE_URL+"/experiments/"+getNthExperimentId(1));

		expect()
		.statusCode(200)
		.body("instances.size", is(1))
		.when()
		.get(BASE_URL+"/experiments/"+getNthExperimentId(2));

		expect()
		.statusCode(200)
		.body("instances.size", is(0))
		.when()
		.get(BASE_URL+"/experiments/"+getNthExperimentId(3));
	}


	@Test
	public void testAddExperiment() throws JSONException{
		//US4.1 Como usuario quiero agregar experimentos
		JSONObject exp = new JSONObject();
		exp.put("name", "mockapi-experience");
		exp.put("description", "test");

		JSONArray deliverables = new JSONArray();
		JSONObject del1 = new JSONObject();
		del1.put("name", "mockups");
		del1.put("type", "zip");
		JSONObject del2 = new JSONObject();
		del2.put("name", "code");
		del2.put("type", "zip");

		deliverables.put(del1);
		deliverables.put(del2);
		exp.put("deliverables", deliverables);


		long id = given().contentType(ContentType.JSON)
		.body(exp.toString())
		.expect()
		.statusCode(200)
		.when()
		.post(BASE_URL+"/experiments")
		.then()
			.extract()
				.jsonPath().getLong("id");

		expect()
		.statusCode(200)
		.body("name", equalTo("mockapi-experience"))
		.body("description", equalTo("test"))
		.body("deliverables.size", is(2))
		.when()
		.get(BASE_URL+"/experiments/"+id);
	}
	@Test
	public void testAddExperimentWithRepeatedDeliverablesShouldFail() throws JSONException{
		//US4.2 Los nombres de entregables deben ser unicos
		JSONObject exp = new JSONObject();
		exp.put("name", "mockapi-experience");
		exp.put("description", "test");

		JSONArray deliverables = new JSONArray();
		JSONObject del1 = new JSONObject();
		del1.put("name", "code");
		del1.put("type", "zip");
		JSONObject del2 = new JSONObject();
		del2.put("name", "code");
		del2.put("type", "txt");

		deliverables.put(del1);
		deliverables.put(del2);
		exp.put("deliverables", deliverables);


		given().contentType(ContentType.JSON)
		.body(exp.toString())
		.expect()
		.statusCode(400)
		.when()
		.post(BASE_URL+"/experiments");

		expect()
		.statusCode(200)
		.body("size", is(4))
		.when()
		.get(BASE_URL+"/experiments");
	}

	@Test
	public void testAddExperimentBadNameDeliverablesShouldFail() throws JSONException{
		//US4.3 Los nombres de entregables deben tener solo caracteres alphanumericos y -
		JSONObject exp = new JSONObject();
		exp.put("name", "mockapi-experience");
		exp.put("description", "test");

		JSONArray deliverables1 = new JSONArray();
		JSONObject del1 = new JSONObject();
		del1.put("name", "code_test");
		del1.put("type", "zip");

		deliverables1.put(del1);
		exp.put("deliverables", deliverables1);


		given().contentType(ContentType.JSON)
		.body(exp.toString())
		.expect()
		.statusCode(400)
		.when()
		.post(BASE_URL+"/experiments");

		JSONArray deliverables2 = new JSONArray();
		JSONObject del2 = new JSONObject();
		del2.put("name", "@test");
		del2.put("type", "zip");

		deliverables2.put(del2);
		exp.put("deliverables", deliverables1);


		given().contentType(ContentType.JSON)
		.body(exp.toString())
		.expect()
		.statusCode(400)
		.when()
		.post(BASE_URL+"/experiments");

		expect()
		.statusCode(200)
		.body("size", is(4))
		.when()
		.get(BASE_URL+"/experiments");
	}

	@Test
	public void testAddSubject() throws JSONException{
		//US7.1 Como usaurio debo poder agregar sujetos
		JSONObject subject = new JSONObject();
		subject.put("name", "name3");
		subject.put("surname", "sname3");
		subject.put("email", "email3@gmail.com");
		given()
		.body(subject.toString())
		.expect()
		.statusCode(200)
		.when()
		.post(BASE_URL+"/subjects");

		expect()
		.body("size", is(3))
		.when()
		.get(BASE_URL+"/subjects");
	}

	@Test
	public void testAddSubjectWithoutNameShouldFail() throws JSONException{
		//US7.2 El campo name es obligatorio
		JSONObject subject = new JSONObject();
		subject.put("surname", "sname3");
		subject.put("email", "email3@gmail.com");
		given()
		.body(subject.toString())
		.expect()
		.statusCode(400)
		.when()
		.post(BASE_URL+"/subjects");

		expect()
		.body("size", is(2))
		.when()
		.get(BASE_URL+"/subjects");
	}

	@Test
	public void testAddSubjectWithoutSurnameShouldFail() throws JSONException{
		//US7.3 Como surname es obligatorio
		JSONObject subject = new JSONObject();
		subject.put("name", "name3");
		subject.put("email", "email3@gmail.com");
		given()
		.body(subject.toString())
		.expect()
		.statusCode(400)
		.when()
		.post(BASE_URL+"/subjects");

		expect()
		.body("size", is(2))
		.when()
		.get(BASE_URL+"/subjects");
	}

	@Test
	public void testAddSubjectWithoutEmailShouldFail() throws JSONException{
		//US7.4 Como email es obligatorio
		JSONObject subject = new JSONObject();
		subject.put("name", "name3");
		subject.put("surname", "sname3");
		given()
		.body(subject.toString())
		.expect()
		.statusCode(400)
		.when()
		.post(BASE_URL+"/subjects");

		expect()
		.body("size", is(2))
		.when()
		.get(BASE_URL+"/subjects");
	}

	@Test
	public void testAddSubjectWithBadEmailShouldFail() throws JSONException{
		//US7.5 Como usaurio debo poder agregar sujetos
		JSONObject subject = new JSONObject();
		subject.put("name", "name3");
		subject.put("surname", "sname3");
		subject.put("email", "email3gmail.com");
		given()
		.body(subject.toString())
		.expect()
		.statusCode(400)
		.when()
		.post(BASE_URL+"/subjects");

		expect()
		.body("size", is(2))
		.when()
		.get(BASE_URL+"/subjects");
	}

	@Test
	public void testDeleteSubject() throws JSONException{
		//US6.2 Como usuario deseo eleminar sujetos de prueba
		expect()
		.statusCode(200)
		.when()
		.delete(BASE_URL+"/subjects/"+getFirstSubjectId());

		expect()
		.body("size", is(1))
		.when()
		.get(BASE_URL+"/subjects");
	}

	@Test
	public void testFileUpload(){
		//US8 Como usuario deseo registrar los resultados de un experimento.
		File file = new File(getClass().getClassLoader().getSystemResource("code.txt").getFile());
		File file2 = new File(getClass().getClassLoader().getSystemResource("doc.txt").getFile());
		given()
		.multiPart("code", file)
		.multiPart("doc", file2)
		.formParam("description", "All ok.")
		.expect()
		.statusCode(200)
		.when()
    .put(BASE_URL+"/instances/"+getFirstInstanceId());

		expect()
		.body("description", equalTo("All ok."))
		.statusCode(200)
		.when()
		.get(BASE_URL+"/instances/"+getFirstInstanceId());

		expect()
		.body(equalTo("This is a test file to be uploaded to the server.\n"))
		.statusCode(200)
		.when()
		.get(BASE_URL+"/instances/"+getFirstInstanceId()+"/deliverables/code");
	}

	@Test
	public void testIncompleteFileUploadShouldFail(){
		//US8.2 Al subir un experimento se deben proveer todos los archivos de entregables
		File file = new File(getClass().getClassLoader().getSystemResource("code.txt").getFile());
		given()
		.multiPart("code", file)
		.formParam("description", "All ok.")
		.expect()
		.statusCode(400)
		.when()
    .put(BASE_URL+"/instances/"+getFirstInstanceId());

		expect()
		.body("description", equalTo(""))
		.statusCode(200)
		.when()
		.get(BASE_URL+"/instances/"+getFirstInstanceId());

		expect()
		.statusCode(404)
		.when()
		.get(BASE_URL+"/instances/"+getFirstInstanceId()+"/deliverables/code");
	}

	@Test
	public void testFileUploadWithoutDescriptionShouldFail(){
		//US8.3 La descripcion del resultado del experimento es obligatoria.
		File file = new File(getClass().getClassLoader().getSystemResource("code.txt").getFile());
		File file2 = new File(getClass().getClassLoader().getSystemResource("doc.txt").getFile());
		given()
		.multiPart("code", file)
		.multiPart("doc", file2)
		.expect()
		.statusCode(400)
		.when()
    .put(BASE_URL+"/instances/"+getFirstInstanceId());

		expect()
		.body("description", equalTo(""))
		.statusCode(200)
		.when()
		.get(BASE_URL+"/instances/"+getFirstInstanceId());

		expect()
		.statusCode(404)
		.when()
		.get(BASE_URL+"/instances/"+getFirstInstanceId()+"/deliverables/code");
	}

		@Test
	public void testFileUploadWithTooLongDescriptionShouldFail(){
		//US8.3 La descripcion del resultado del experimento tiene un limite de 200 caracteres..
		File file = new File(getClass().getClassLoader().getSystemResource("code.txt").getFile());
		File file2 = new File(getClass().getClassLoader().getSystemResource("doc.txt").getFile());
		given()
		.multiPart("code", file)
		.multiPart("doc", file2)
		.formParam("description", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed molestie tellus vel auctor eleifend. Donec vel vestibulum ante, et sodales metus. Mauris egestas arcu ut fermentum pulvinar. Lorem ipsum dolor sit amet, consectetur adipiscing elit.")
		.expect()
		.statusCode(400)
		.when()
    .put(BASE_URL+"/instances/"+getFirstInstanceId());

		expect()
		.body("description", equalTo(""))
		.statusCode(200)
		.when()
		.get(BASE_URL+"/instances/"+getFirstInstanceId());

		expect()
		.statusCode(404)
		.when()
		.get(BASE_URL+"/instances/"+getFirstInstanceId()+"/deliverables/code");
	}
}
